﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour {

	void Start () {
		this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 500);
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Bloque") {
			other.GetComponent<Bloque> ().bajarGolpes ();
			Destroy (gameObject);
		} else if (other.tag == "Techo") {
			Destroy (gameObject);
		}
	}
}