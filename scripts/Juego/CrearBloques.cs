using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrearBloques : MonoBehaviour {

    public GameObject bloque;
	public Player playerScript;
    public static int nivel;

    private Camera camara;
    private SpriteRenderer bloqueSprite;
    private GameObject bloqueAux;
    private float height;
    private float width;
    private float tamBloqueX;
    private float tamBloqueY;
    private float desplazamientoX;
    private float desplazamientoY;
    private float minX;
    private float minY;
	private float maxX;
    private float baseY;
    private float auxX;
	private float auxY;
	private float toX;
    private int color;
    private int golpes;
    private int maxColor;
    private int maxGolpes;
    private int minColor;
    private int minGolpes;
	private int cantGolpes;
	private int golpesParaBajar;
	private bool derrota;

    private void Awake() {
        bloqueSprite = bloque.GetComponent<SpriteRenderer>();
    }

    void Start () {
        Transform techo = GameObject.FindGameObjectWithTag("Techo").transform;
		//se obtiene la camara principal
		camara = Camera.main;
        //se obtiene el tamaño
       	height = 2f * camara.orthographicSize;
		width = height * camara.aspect;
        //se obtiene el tamaño de los bloques
		tamBloqueX = bloque.transform.localScale.x * bloqueSprite.size.x;
		tamBloqueY = bloque.transform.localScale.y * bloqueSprite.size.y;
        //se calcula los mínimos
		minX = -width/2 + tamBloqueX/2;
        maxX = width/2 - tamBloqueX/2;
		baseY = techo.position.y - techo.localScale.y / 2 - tamBloqueY/2;
        minColor = 0;
        maxColor = 7;   //el 7 no es incluido
        minGolpes = 1;
        maxGolpes = 4;  //el 4 no es incluido
		cantGolpes = 0;
		golpesParaBajar = 4;
        doLevel (nivel);
	}

	public void nextLevel() {
		doLevel (++nivel);
	}

	public void doLevel(int nivel) {
		//limpia los bloques existentes
		var colBloques = GameObject.FindGameObjectsWithTag ("Bloque");
		foreach (var bloqueAux in colBloques) {
			Destroy(bloqueAux);
		}
		switch (nivel) {
		case 0:	//nivel infinito
			//crea 4 filas de una cantidad random de bloques y cada 4 golpes con el player, bajan los bloques y agrega una nueva fila
			derrota = false;
			minY = baseY - 4 * tamBloqueY;
			for (auxY = baseY; auxY > minY; auxY -= tamBloqueY) {
				crearFilaRandom (auxY);
			}
			minY = baseY - 25 * tamBloqueY;
			break;
	    case 1:	//2 filas pegadas
		    desplazamientoY = tamBloqueY;
		    minY = 2 * desplazamientoY;
		    for (float y = 0; y < minY; y += desplazamientoY) {
			    crearFilaDe11 (baseY - y);
		    }
		    break;
	    case 2:	//5 filas separadas por 1 espacio
		    desplazamientoY = 2 * tamBloqueY;
		    minY = 5 * desplazamientoY;
		    for (float y = 0; y < minY; y += desplazamientoY) {
			    crearFilaDe11 (baseY - y);
		    }
		    break;
	    case 3:	//11 filas pegadas de 6 bloques separados por 1 espacio
		    desplazamientoY = tamBloqueY;
		    minY = 11 * desplazamientoY;
		    for (float y = 0; y < minY; y += desplazamientoY) {
			    crearFilaDe6 (baseY - y);
		    }
		    break;
        case 4: //11 filas pegadas de 1 bloque a 11 bloques
            desplazamientoY = tamBloqueY;
            for (int i = 0; i < 11; i++) {
                crearFilaDe_desde_en(i+1,minX,baseY - i*desplazamientoY);
            }
            break;
        case 5: //10 filas de (2,3,4,...,10,11,10,...,4,3,2) bloques seguidos
            int aux = 5;
            float minXAux = minX + tamBloqueX;
            desplazamientoY = tamBloqueY;
            for (int i = 0; i < 5; i++) {
                //es el doble del numero de fila si empezara en 1
                crearFilaDe_desde_en((i + 1) * 2, minX, baseY - i * desplazamientoY);
            }
            for (int i = 5; i < 10; i++) {
                //es el doble del numero de fila si empezara en 1
                crearFilaDe_desde_en(aux*2, minXAux, baseY - i * desplazamientoY);
                aux--;
                minXAux += 2 * tamBloqueX;
            }
            break;
        case 6: //un punto adentro de un rectangulo adentro de otro rectangulo
            //rectangulo exterior
			auxY = baseY - 2 * tamBloqueY;
            //crea la linea superior
            auxX = minX + tamBloqueX;
            crearFilaDe_desde_en(9, auxX, auxY);
            //crea 7 filas de 2 elementos separados
			auxY -= tamBloqueY;
			minY = 7 * tamBloqueY;
			for (float y = 0; y < minY; y += tamBloqueY) {
                crearFilaDe2Desde_separadosPor_BloquesEn(auxX, 7, auxY - y);
            }
            //crea la linea inferior
            crearFilaDe_desde_en(9, auxX, auxY - minY);

            //rectangulo interior
			auxY = baseY - 4 * tamBloqueY;
            //crea la linea superior
            auxX = minX + 3 * tamBloqueX;
            crearFilaDe_desde_en(5, auxX, auxY);
            //crea 7 filas de 2 elementos separados
			auxY -= tamBloqueY;
			minY = 3 * tamBloqueY;
			for (float y = 0; y < minY; y += tamBloqueY)
            {
                crearFilaDe2Desde_separadosPor_BloquesEn(auxX, 3, auxY - y);
            }
            //crea la linea inferior
            auxY -= minY;
            crearFilaDe_desde_en(5, auxX, auxY);

            //crea punto central
            crearBloque(auxX + 2 * tamBloqueX, auxY + 2 * tamBloqueY);
            break;
        case 7: //3 rectangulos con 2 barras
            auxY = baseY - 2 * tamBloqueY;
            //crea fila de 9
            auxX = minX + tamBloqueX;
            crearFilaDe_desde_en(9, auxX, auxY);
            //crea fila de 2 elementos separados por 8 bloques con un bloque en el medio
            auxY -= tamBloqueY;
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX, 7, auxY);
            crearBloque(auxX + 4 * tamBloqueX, auxY);
            //crea fila de 9
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(9, auxX, auxY);
            //crea 3 filas de 2 bloques separados por 7 y 2 bloques separados por 3
            auxY -= tamBloqueY;
            minY = auxY - 3 * tamBloqueY;
            for (auxY = auxY; auxY > minY; auxY -= tamBloqueY) {
                crearFilaDe2Desde_separadosPor_BloquesEn(auxX, 7, auxY);
                crearFilaDe2Desde_separadosPor_BloquesEn(auxX+2*tamBloqueX, 3, auxY);
            }
            //crea fila con 2 bloques separados por 7 y 5 bloques seguidos en el medio
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX, 7, auxY);
            crearFilaDe_desde_en(5,auxX+2*tamBloqueX,auxY);
            //crea fila de 2 bloques separados por 7
            auxY -= tamBloqueY;
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX, 7, auxY);
            break;
        case 8: //1 linea y 8 cajas
            auxY = baseY;
            //crea fila de 11 bloques
            crearFilaDe11(auxY);
            //deja una fila vacia y crea 3 cajas de 3x3
            auxY -= 2 * tamBloqueY;
            minY = auxY - 3 * tamBloqueY;
            for (auxY = auxY; auxY > minY; auxY -= tamBloqueY) {
                crearFilaDe_desde_en(3, minX, auxY);
                crearFilaDe_desde_en(3, minX+4*tamBloqueX, auxY);
                crearFilaDe_desde_en(3, minX+8*tamBloqueX, auxY);
            }
            //deja una fila vacia y crea 2 cajas de 3x3
            auxY -= tamBloqueY;
            minY = auxY - 3 * tamBloqueY;
            for (auxY = auxY; auxY > minY; auxY -= tamBloqueY)
            {
                crearFilaDe_desde_en(3, minX + 2 * tamBloqueX, auxY);
                crearFilaDe_desde_en(3, minX + 6 * tamBloqueX, auxY);
            }
            //deja una fila vacia y crea 3 cajas de 3x3
            auxY -= tamBloqueY;
            minY = auxY - 3 * tamBloqueY;
            for (auxY = auxY; auxY > minY; auxY -= tamBloqueY)
            {
                crearFilaDe_desde_en(3, minX, auxY);
                crearFilaDe_desde_en(3, minX + 4 * tamBloqueX, auxY);
                crearFilaDe_desde_en(3, minX + 8 * tamBloqueX, auxY);
            }
            break;
        case 9: //crea una bola de 7x12
            auxY = baseY - 2 * tamBloqueY;
            auxX = minX + 2 * tamBloqueX;
            //crea fila de 3 bloques seguidos
            crearFilaDe_desde_en(3, auxX + 2 * tamBloqueX, auxY);
            //crea 2 filas de 5 bloques seguidos
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(5, auxX + tamBloqueX, auxY);
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(5, auxX + tamBloqueX, auxY);
            //crea 6 filas de 7 bloques seguidos
            auxY -= tamBloqueY;
            minY = auxY - 6 * tamBloqueY;
            for (auxY = auxY; auxY > minY; auxY -= tamBloqueY) {
                crearFilaDe_desde_en(7, auxX, auxY);
            }
            //crea 2 filas de 5 bloques seguidos
            crearFilaDe_desde_en(5, auxX + tamBloqueX, auxY);
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(5, auxX + tamBloqueX, auxY);
            //crea fila de 3 bloques seguidos
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(3, auxX + 2 * tamBloqueX, auxY);
            break;
        case 10:    //crea 2 ojos y una boca
            auxY = baseY - tamBloqueY;
            auxX = minX + tamBloqueX;
            //crea una fila con 4 bloques separados
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX, 7, auxY);
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX+2*tamBloqueX, 3, auxY);
            //crea 2 cajas de 3x3
            auxY -= tamBloqueY;
            minY = auxY - 3 * tamBloqueY;
            for (auxY = auxY; auxY > minY; auxY -= tamBloqueY) {
                crearFilaDe_desde_en(3, auxX, auxY);
                crearFilaDe_desde_en(3, auxX + 6 * tamBloqueX, auxY);
            }
            //deja una fila vacia y crea una caja de 5x6
            auxY -= tamBloqueY;
            minY = auxY - 6 * tamBloqueY;
            for (auxY = auxY; auxY > minY; auxY -= tamBloqueY)
            {
                crearFilaDe_desde_en(5, auxX+2*tamBloqueX, auxY);
            }
            break;
        case 11:    //crea un bicho con antenas
            auxY = baseY - tamBloqueY;
            auxX = minX + tamBloqueX;
            //crea fila de 2 bloques separados por 3
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX + 2 * tamBloqueX, 3, auxY);
            //crea 2 filas de 2 bloques separados por 1
            auxY -= tamBloqueY;
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX + 3 * tamBloqueX, 1, auxY);
            auxY -= tamBloqueY;
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX + 3 * tamBloqueX, 1, auxY);
            //crea 2 filas de 5 bloques seguidos
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(5, auxX + 2 * tamBloqueX, auxY);
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(5, auxX + 2 * tamBloqueX, auxY);
            //crea 2 filas de 7 bloques seguidos
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(7, auxX + tamBloqueX, auxY);
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(7, auxX + tamBloqueX, auxY);
            //crea 2 filas de 9 bloques seguidos
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(9, auxX, auxY);
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(9, auxX, auxY);
            //crea fila de 2 bloques separados y 5 seguidos
            auxY -= tamBloqueY;
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX, 7, auxY);
            crearFilaDe_desde_en(5, auxX + 2*tamBloqueX, auxY);
            //crea 2 filas de 4 bloques separados
            auxY -= tamBloqueY;
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX + 3, 7, auxY);
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX + 2 * tamBloqueX, 3, auxY);
            auxY -= tamBloqueY;
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX + 3, 7, auxY);
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX + 2 * tamBloqueX, 3, auxY);
            //crea 2 filas de 2 bloques separados
            auxY -= tamBloqueY;
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX + 3 * tamBloqueX, 1, auxY);
            auxY -= tamBloqueY;
            crearFilaDe2Desde_separadosPor_BloquesEn(auxX + 3 * tamBloqueX, 1, auxY);
            break;
        case 12:    //crea una casa
            auxY = baseY - tamBloqueY;
            auxX = minX + tamBloqueX;
            //crea un bloque
            crearBloque(auxX + 4 * tamBloqueX, auxY);
            //crea 3 bloques seguidos y 1 solo
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(3, auxX + 3 * tamBloqueX, auxY);
            crearBloque(auxX + 8 * tamBloqueX, auxY);
            //crea 5 bloques seguidos y 1 solo
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(5, auxX + 2 * tamBloqueX, auxY);
            crearBloque(auxX + 8 * tamBloqueX, auxY);
            //crea 8 bloques seguidos
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(8, auxX + tamBloqueX, auxY);
            //crea 9 bloques seguidos
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(9, auxX, auxY);
            //crea 2 filas de 8 bloques seguidos
            auxX += tamBloqueX;
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(8, auxX, auxY);
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(8, auxX, auxY);
            //crea 2 filas de 3 bloques seguidos y otros 4 bloques seguidos
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(3, auxX, auxY);
            crearFilaDe_desde_en(4, auxX + 4 * tamBloqueX, auxY);
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(3, auxX, auxY);
            crearFilaDe_desde_en(4, auxX + 4 * tamBloqueX, auxY);
            //crea 3 filas de 8 bloques seguidos
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(8, auxX, auxY);
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(8, auxX, auxY);
            auxY -= tamBloqueY;
            crearFilaDe_desde_en(8, auxX, auxY);
            break;
        }
	}

	public bool isInfinite() {
		return (nivel == 0);
	}

	public void colisionConPlayer(){
		if (isInfinite ()) {
			cantGolpes++;
			if (cantGolpes == golpesParaBajar) {
				cantGolpes = 0;
				bajarBloques ();
				crearFilaRandom (baseY);
				if (derrota) {
					playerScript.perder ();
				}
			}
		}
	}

    //CREADORES DE FILAS

    private void crearFilaDe11(float y) {
		//crea 11 bloques seguidos en la misma fila
		desplazamientoX = tamBloqueX;
		toX = minX + 11 * desplazamientoX;
		for(float x = minX; x <= toX; x += desplazamientoX) {
            crearBloque(x, y);
        }
  }

    private void crearFilaDe6(float y) {
		//crea 6 bloques en una fila, separados por un espacio
		desplazamientoX = 2*tamBloqueX;
		toX = minX + 6 * desplazamientoX;
		for(float x = minX; x < toX; x += desplazamientoX) {
            crearBloque(x, y);
        }
    }

    private void crearFilaDe_desde_en(int cant, float minX, float y) {
        //Crea una fila de cant bloques seguidos desde minX en la posicion y
        desplazamientoX = tamBloqueX;
		toX = minX + cant * desplazamientoX;
		for (float x = minX; x < toX; x += desplazamientoX) {
            crearBloque(x, y);
        }
    }

    private void crearFilaDe2Desde_separadosPor_BloquesEn(float minX, int bloquesSeparacion, float y) {
        //crea 2 bloques en la misma fila, el primero en minX y el segundo separado por una cantidad de bloques igual a bloquesSeparacion en la posicion y
		toX = minX + bloquesSeparacion * tamBloqueX + tamBloqueX;
        //primer bloque
        crearBloque(minX, y);
        //segundo bloque
		crearBloque(toX, y);
    }

	private void crearFilaRandom(float y) {
		//crea una cantidad de bloques totalmente random
		for (float x = minX ; x <= maxX ; x += tamBloqueX) {
			if (randomBool ()) {
				crearBloque (x, y);
			}
		}
	}

    private void crearBloque(float x, float y) {
        bloqueAux = Instantiate(bloque, new Vector2(x, y), Quaternion.identity);
        color = Random.Range(minColor, maxColor);
        golpes = Random.Range(minGolpes, maxGolpes);
        bloqueAux.GetComponent<Bloque>().setColorAndLife(color, golpes);
    }

	private bool randomBool() {
		return (Random.Range(0, 2) == 1);
	}

	private void bajarBloques() {
		var colBloques = GameObject.FindGameObjectsWithTag ("Bloque");
		foreach (var bloqueAux in colBloques) {
			bloqueAux.transform.position = new Vector2 (bloqueAux.transform.position.x, bloqueAux.transform.position.y - tamBloqueY);
			if (bloqueAux.transform.position.y <= minY) {
				derrota = true;
			}
		}
	}
}
