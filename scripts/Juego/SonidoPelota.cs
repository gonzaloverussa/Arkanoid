﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SonidoPelota : MonoBehaviour
{
    public Button myButton;
    public Sprite soundOn;
    public Sprite soundOff;
    public AudioSource pared;
    public AudioSource bloque;
    public AudioSource piso;
    private bool pause = false;//=usuario.sonidojuego();

    private void Start()
    {
     /*   pared = pared.GetComponent<AudioSource>();
        bloque = bloque.GetComponent<AudioSource>();
        piso = piso.GetComponent<AudioSource>();*/
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bloque"))
        {
            bloque.Play();
        }
        if (collision.gameObject.CompareTag("Piso"))
        {
            piso.Play();
        }
        if (collision.gameObject.CompareTag("Pared") || collision.gameObject.CompareTag("Techo") || collision.gameObject.CompareTag("Player"))
        {
            pared.Play();
        }
    }
    public void pausaSonidos()
    {
        if (pause)
        {
            myButton.image.overrideSprite = soundOn;
        } else
        {
            myButton.image.overrideSprite = soundOff;
        }
        pause = !pause;
        pared.mute = !pared.mute;
        bloque.mute = !bloque.mute;
        piso.mute = !piso.mute;
    }

}
