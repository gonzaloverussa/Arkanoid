﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pelota : MonoBehaviour {
	public GameObject player;
	public CrearBloques crearBolque;

	private Rigidbody2D rbPlayer;
	private Rigidbody2D rb;
	private int velocidad;
	private int velInicial;
	private int velFinal;
	private int aceleracion;
	private Vector2 direccionDeLlegada;
	private bool colisionando;
	private float distanciaPorcentual;
	private float posYInicial;
	private float acelerado;

	void Start () {
		rb = GetComponent<Rigidbody2D>();
		rbPlayer = player.GetComponent<Rigidbody2D>();
		velInicial = 600;
		velocidad = velInicial;
		velFinal = 1600;
		aceleracion = 20;
		posYInicial = rb.position.y;
        rb.isKinematic = true;
		acelerado = 1;
	}

	public void lanzar() {
		rb.isKinematic = false;
		velocidad = velInicial + (velocidad - velInicial) / 2;
		rb.velocity = new Vector2(1,1) * velocidad;
		colisionando = false;
	}

	public void destroy() {
		Destroy(gameObject);
	}

	public void pararPelota() {
		//se coloca la pelota en la posicion del jugador sumando la mitad del tamaño en y del jugador y la pelota
		rb.isKinematic = true;
		rb.velocity = Vector2.zero;
		transform.position = new Vector3 (player.transform.position.x, posYInicial, this.transform.position.z);
	}

	public void separar() {
		player.transform.DetachChildren ();
	}

	public void unir() {
		this.transform.parent = player.transform;
	}

	public void acelerar(bool acelerar) {
		if (acelerar) {
			acelerado = 2;
		} else {
			acelerado = 1;
		}
	}

	private void OnCollisionEnter2D(Collision2D other) {
		colisionando = true;
		if (other.collider.tag == "Techo" || other.collider.tag == "Pared") {
			//si la colision es con un limite, se verifica que el movimiento no quede horizontal o vertical
			if (Mathf.Abs (rb.velocity.x) < Mathf.Abs (rb.velocity.y)/8) {
				//el movimiento es casi vertical, se modifica el angulo
				if (other.collider.tag == "Pared") {
					//se cambia el sentido del eje x para que "rebote" en la pared
					if (direccionDeLlegada.x > 0) {
						//iba hacia la derecha, ahora va hacia la izquierda
						rb.velocity = new Vector2 (-Mathf.Abs (rb.velocity.y) / 5f, rb.velocity.y);
					} else {
						//iba hacia la izquierda, ahora va hacia la derecha
						rb.velocity = new Vector2 (Mathf.Abs (rb.velocity.y) / 5f, rb.velocity.y);
					}
				} else {
					//colisiono con el techo, mantiene el sentido del eje x
					rb.velocity = new Vector2 (rb.velocity.y / 5f, rb.velocity.y);
				}
			}
			if (Mathf.Abs (rb.velocity.y) < Mathf.Abs (rb.velocity.x)/8) {
				//el movimiento es casi horizontal, se modifica el angulo
				if (other.collider.tag == "Techo") {
					//cambia el sentido del eje y para que "rebote" en el techo
					if (direccionDeLlegada.y > 0) {
						//iba hacia arriba, ahora va hacia abajo
						rb.velocity = new Vector2 (rb.velocity.x, -Mathf.Abs (rb.velocity.x) / 5f);
					}
				} else {
					//colisiono con la pared, mantiene el sentido del eje y
					rb.velocity = new Vector2 (rb.velocity.x, rb.velocity.x / 5f);
				}
			}
		}
		if (other.collider.tag == "Player") {
			crearBolque.colisionConPlayer ();
			distanciaPorcentual = (rb.position.x - rbPlayer.position.x) / (rbPlayer.transform.localScale.x/2);
			rb.velocity = new Vector2 (distanciaPorcentual * 2, 1);	//se da angulo segun donde choca en el player
			if (velocidad < velFinal) {
				velocidad += aceleracion;	//se incrementa la velocidad en base de la aceleracion
			}
			this.ajustarVelocidad ();
		}
	}

	private void OnCollisionExit2D(Collision2D other){
		colisionando = false;
	}

	private void Update() {
		if (rb.isKinematic) {
			//Esta detenida en el player
			if (Input.touchCount > 0) {
				Touch firstTouch = Input.GetTouch (0);
				if (firstTouch.phase == TouchPhase.Ended) {
					//se solto el dedo
					this.lanzar ();
				}
			} else {
				if (Input.GetButton("Fire1"))
				{
					this.lanzar();
				}
			}
		}
		if (!colisionando) {
			direccionDeLlegada = rb.velocity.normalized;
		}
		this.ajustarVelocidad ();
	}

	private void ajustarVelocidad() {
		rb.velocity = rb.velocity.normalized * velocidad * acelerado;
		if (Input.touchCount > 1) {
			//si se está pulsando con 2 o más dedos, el juego se acelera
			rb.velocity *= 2;
		}
	}
}