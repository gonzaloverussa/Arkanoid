﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BarraDePuntos : MonoBehaviour {
    public Image[] vidas;
    private int cantidadVidas=3;
    private string puntos;
    public Text puntaje;
    public MenuJuego menues;
    public Main funciones;
	// Use this for initialization
	 void Start () {
		
	}
    public void perderVida() {
        cantidadVidas--;
        print("perder vida"+cantidadVidas.ToString());
        vidas[cantidadVidas].gameObject.SetActive(false);

        

    }
    public void partidaPerdida(int puntaje) {

        bool record;
        record = funciones.actualizarPuntos(puntaje);
        
        menues.perderPartida(record);
    }
    public void actualizarPuntaje(int puntaje) {
        this.puntaje.text = puntaje.ToString();
    }

    public void partidaGanada(int puntaje) {
        bool record;
        record = funciones.actualizarPuntos(puntaje);
        funciones.ganarNivel();
        menues.ganarPartida(record);
    }
    // Update is called once per frame
    void Update () {
		
	}
}
