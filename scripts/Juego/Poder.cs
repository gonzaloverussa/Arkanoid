﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poder : MonoBehaviour {

	public Player playerScript;

	private Rigidbody2D rb;
	private bool activo;
	public float time;
	private int poder;

	void Start() {
		rb = GetComponent<Rigidbody2D>();
		rb.velocity = new Vector2 (0, -250);
		activo = false;
		time = 7;
	}

	public void setPoder(int poder) {
		this.poder = poder;
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player") {
			playerScript = other.gameObject.GetComponent<Player> ();
			Timer aux = other.gameObject.GetComponent<Timer>();
			if (aux != null) {
				Destroy (aux);
			}
			asignarPoder ();
			other.gameObject.AddComponent<Timer> ();
			Destroy(gameObject);
		} else if (other.tag == "Piso") {
			Destroy (gameObject);
		}
	}

	private void asignarPoder() {
		switch (poder) {
		case 0:	//agranda la barra
			playerScript.agrandar ();
			break;
		case 1:	//achica la barra
			playerScript.achicar ();
			break;
		case 2:	//acelera la pelota
			playerScript.acelerarPelota();
			break;
		case 3:
			playerScript.dispararBalas ();
			break;
		}
	}
}
