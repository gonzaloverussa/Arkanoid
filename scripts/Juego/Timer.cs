﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {

	private Player playerScript;
	private float time;

	// Use this for initialization
	void Start () {
		print ("inicializa timer");
		playerScript = this.GetComponent<Player> ();
		time = 7;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		time -= Time.fixedDeltaTime;
		if (time <= 0) {
			playerScript.resetearPoderes ();
			Destroy (this);
		}
	}

	void OnDestroy() {
//		playerScript.resetearPoderes ();
		print ("El timer " + this.name + " finalizó con " + time.ToString());
	}
}
