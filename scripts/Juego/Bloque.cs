﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bloque : MonoBehaviour {
    public Sprite[] colSprite;
	public GameObject poder;

    private int golpesNecesarios;
    private Player scriptPlayer;
    private int puntos;
    private SpriteRenderer sprite;
    private int baseColor;
    public GameObject efectoParticula;
    public Material[] colores;
    private int numColor = 0;
	private int poderRandom;

    private void Awake() {
        sprite = this.GetComponent<SpriteRenderer>();
        scriptPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    public void setColorAndLife(int color, int life) {
        //0: Blue   1:Green     2:Orange    3:Pink  4:Red   5:Violet    6:Yellow
        golpesNecesarios = life;
        puntos = golpesNecesarios * 100;
        baseColor = color * 3;
        numColor = color;
        sprite.sprite = colSprite[baseColor + golpesNecesarios - 1];    //-1 porque arranca en 0
    }

	public void bajarGolpes() {
		golpesNecesarios--;
		if (golpesNecesarios == 0) {
			destruir();
		} else {
			sprite.sprite = colSprite[baseColor + golpesNecesarios - 1];    //-1 porque arranca en 0
		}
	}

    private void OnCollisionEnter2D(Collision2D col) {
		if (col.collider.tag == "Pelota" || col.collider.tag == "Bala") {
			bajarGolpes ();
		}
	}

    private void destruir() {
        gameObject.SetActive(false);

		poderRandom = Random.Range (0, 11);	//el ultimo numero no se incluye
		if (poderRandom < 4) {
			GameObject poderAux = Instantiate (poder, transform.position, Quaternion.identity);
			poderAux.GetComponent<Poder>().setPoder (poderRandom);
			poderAux.GetComponent<Renderer>().material = colores[poderRandom];
		}
        crearParticulas();
        Destroy(gameObject);
        scriptPlayer.sumarPuntos(puntos);
    }

    private void crearParticulas() {
        GameObject go =Instantiate(efectoParticula, transform.position, Quaternion.identity);//instancio efecto de particula en la posicion del bloque con la rotacion por defecto.
        go.GetComponent<Renderer>().material = colores[numColor];
    }
}