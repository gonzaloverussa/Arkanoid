﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {
	public Pelota pelotaScript;
    public BarraDePuntos barraDePuntos;
	public CrearBloques crearBolque;
    public AudioSource sonidoMuerte;
	public GameObject bala;

	private SpriteRenderer sprite;
	private Vector3 posBalaLeft;
	private Vector3 posBalaRight;
	private float fuerza;
	private float velocidad;
    private Rigidbody2D rb;
	private int cantVidas;
	private float posDedo;
	private float distancia;
	private int direccion;
	private Camera camara;
	private float limite;
	private float sensibilidad;
    private int puntaje;
	private bool infinito;
	private float tamX;
	private float tamY;
	private Vector3 tam;

	// Use this for initialization
	void Start() {
		rb = GetComponent<Rigidbody2D>();
		tam = rb.transform.localScale;
		tamX = rb.transform.localScale.x;
		tamY = rb.transform.localScale.y;
		//se obtiene la camara principal
		camara = Camera.main;
		//se calcula el limite dependiendo de la camara y el ancho del player
		limite = camara.orthographicSize * camara.aspect - tamX/2;
		//Se calcula la sensibilidad para controlar el palyer
		sensibilidad = camara.orthographicSize * camara.aspect / 5;
		infinito = crearBolque.isInfinite ();
		fuerza = 1000000;
		velocidad = 2;
		cantVidas = 3;
		distancia = 0;
		direccion = 0;
		sprite = this.GetComponent<SpriteRenderer> ();
		posBalaLeft = Vector3.up * transform.localScale.y / 5 - Vector3.left * transform.localScale.x / 2 * sprite.size.x * 0.8f;
		posBalaRight = Vector3.up * transform.localScale.y / 5 + Vector3.left * transform.localScale.x / 2 * sprite.size.x * 0.8f;
	}

	// Update is called once per frame
	void FixedUpdate() {
		if (Input.GetAxis("Horizontal") != 0) {
			Vector2 vector = new Vector2(Input.GetAxis("Horizontal"), 0);

			rb.AddForce(vector * fuerza);
			rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -velocidad, velocidad), 0);

			//transform.position = new Vector2(Mathf.Clamp(transform.position.x, -7.195f, 7.195f), transform.position.y);
		}
		else {
			rb.velocity = new Vector2(0, 0);
		}

		//Movimiento para android
		if (Input.touchCount > 0) {
			posDedo = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position).x;
			distancia = posDedo - rb.position.x;
			if (Mathf.Abs (distancia) > sensibilidad) {
				//agregamos fuerza
				if (distancia > sensibilidad) {
					direccion = 1;
				} else {
					direccion = -1;
				}
				Vector2 vector = new Vector2 ((float)direccion, 0);
				rb.AddForce (vector * fuerza);
				rb.velocity = new Vector2 (Mathf.Clamp (rb.velocity.x, -velocidad, velocidad), 0);
				if (direccion > 0) {
					rb.position = new Vector2 (Mathf.Clamp (rb.position.x, rb.position.x, posDedo), rb.position.y);
				} else {
					rb.position = new Vector2 (Mathf.Clamp (rb.position.x, posDedo, rb.position.x), rb.position.y);
				}
			} else {
				//cambiamos posicion
				rb.position = new Vector2 (Mathf.Clamp (Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position).x, -limite, limite), rb.position.y);
			}
		}
	}

	public void sumarPuntos(int puntos) {
		puntaje += puntos;
		barraDePuntos.actualizarPuntaje (puntaje);
		if (!infinito) {
			var colBloques = GameObject.FindGameObjectsWithTag ("Bloque");
			if (colBloques.Length == 0) {
				//No quedan bloques que destruir, se gano el nivel
				pelotaScript.destroy ();
				puntaje += cantVidas * 1000;
				barraDePuntos.actualizarPuntaje (puntaje);
				barraDePuntos.partidaGanada(puntaje);
			}
		}
    }

    public void perderVida() {
		cantVidas--;
        barraDePuntos.perderVida();
		destruirPoderes ();
		if (cantVidas == 0) {
			perder();
		} else {
			pelotaScript.pararPelota();
		}
	}

	public void perder() {
        
        sonidoMuerte.Play();
		pelotaScript.destroy();
        barraDePuntos.partidaPerdida(puntaje);
    }

    public void pausaSonido() {
        sonidoMuerte.mute = !sonidoMuerte.mute;
    }

	//PODERES
	public void agrandar() {
		//alarga la barra
		resetearPoderes();
		pelotaScript.separar();
		this.transform.localScale = new Vector2(tamX * 1.5f, tamY); ;//+ new Vector2(tamX / 2, 0);//new Vector2(tamX * 1.5f, tamY);
		pelotaScript.unir ();
	}

	public void volverTamanio() {
		//vuelve la barra a su tamanio original
		pelotaScript.separar();
		this.transform.localScale = tam;//new Vector2(tamX, tamY);
		pelotaScript.unir ();
	}

	public void achicar(){
		//acorta la barra
		resetearPoderes();
		pelotaScript.separar();
		this.transform.localScale = new Vector2(tamX * 0.5f, tamY);
		pelotaScript.unir ();
	}

	public void acelerarPelota() {
		resetearPoderes();
		pelotaScript.acelerar (true);
	}

	public void dispararBalas() {
		resetearPoderes();
		InvokeRepeating ("disparar2Balas", 0, 0.5f);
	}

	public void resetearPoderes() {
		Destroy(Component.FindObjectOfType<Timer> ());	//destruye el timer anterior
		pelotaScript.separar();
		this.transform.localScale = tam;
		pelotaScript.unir ();
		pelotaScript.acelerar (false);
		CancelInvoke ("disparar2Balas");
	}

	private void destruirPoderes() {
		var colPoderes = GameObject.FindGameObjectsWithTag ("Poder");	//poderes cayendo
		foreach (var poderAux in colPoderes) {
			Destroy(poderAux);
		}
		resetearPoderes ();
	}

	private void disparar2Balas() {
		Instantiate (bala, transform.position + posBalaLeft, Quaternion.identity);
		Instantiate (bala, transform.position + posBalaRight, Quaternion.identity);
	}
}