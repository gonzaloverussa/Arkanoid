﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piso : MonoBehaviour {

	public Player playerScript;

	private void OnCollisionEnter2D(Collision2D other) {
		if (other.collider.tag == "Pelota") {
			playerScript.perderVida();
		}
	}
}
