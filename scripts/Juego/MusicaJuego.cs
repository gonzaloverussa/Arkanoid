using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MusicaJuego : MonoBehaviour {
  
	Object[] myMusic; // declare this as Object array
	private AudioSource audio;
	public Button myButton;
    public Sprite soundOn;
    public Sprite soundOff;
    bool music_on = true;

     
     void Awake () {
        audio = GetComponent<AudioSource>();
        myMusic = Resources.LoadAll("Musica/Juego", typeof(AudioClip));
        audio.clip = myMusic[0] as AudioClip;
     }
     
     void Start (){
        audio.Play(); 
     }

		public void pausar(){
        if (music_on)
        {//si esta reproduciendose el sonido
            audio.Pause();
            myButton.image.overrideSprite = soundOff;
        }
        else
        {
            audio.UnPause();
            myButton.image.overrideSprite = soundOn;
        }
        music_on = !music_on;
    }
  
     // Update is called once per frame
     void Update () {
        if(!audio.isPlaying && music_on
            )
          playRandomMusic();
     }
     
     void playRandomMusic() {
        audio.clip = myMusic[Random.Range(0,myMusic.Length)] as AudioClip;
        audio.Play();
     }
 }
