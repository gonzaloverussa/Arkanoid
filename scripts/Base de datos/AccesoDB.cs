﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using System.Data;
using System.Text;
using Mono.Data.SqliteClient;


public class AccesoDB : MonoBehaviour {
    private string connection;
    private IDbConnection dbcon;
    private IDbCommand dbcmd;
    private IDataReader reader;
    private StringBuilder builder;
		public Text mensaje;
		private string consulta;


    public void OpenDB() {
        string db = "arkanoid.db";
        //string filepath = Application.dataPath + "/arkanoid.db";
        string filepath = Application.persistentDataPath + "/" + db;
        print(filepath);
        if (!File.Exists(filepath))
        {
            
            WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + db);
            while (!loadDB.isDone) {

            }
                File.WriteAllBytes(filepath, loadDB.bytes);
        }

        connection = "URI=file:" + filepath;
        Debug.Log("Stablishing connection to: " + connection);
	
        dbcon = new SqliteConnection(connection);
        dbcon.Open();
    }

    public void CloseDB() {
     
        dbcmd.Dispose();
        dbcmd = null;

        dbcon.Close();
        dbcon = null;
    }
   
    public string retornarUsuario(string usuario) {
        usuario = "\"" + usuario + "\"";
				string res = "";
        consulta = "";
        consulta = "SELECT "+ Usuario.PASS +
            "       FROM "+ Tabla.USUARIO + " " +
            "       WHERE "+Usuario.NOMBRE +" = " + usuario;



        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;
        reader = dbcmd.ExecuteReader();
       
        if (reader.Read()) {
            res = reader.GetString(0);
        }

        reader.Close();
        reader = null;
        return res;
    }

    public Boolean existeUsuario(string usuario) {
		usuario = "\"" + usuario + "\"";
        Boolean exito = false;
        consulta = "SELECT 1 " +
            "       FROM "+  Tabla.USUARIO +
            "       WHERE "+ Usuario.NOMBRE +" = " + usuario;

        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;
        reader = dbcmd.ExecuteReader();

        exito = reader.Read();

        reader.Close();
        reader = null;
        return exito;
    }
		public List<PuntosUsuario> obtenerPuntajes(){
		List<PuntosUsuario> ranking = new List<PuntosUsuario>();
        string consulta = "SELECT a.nombre, sum(a.puntaje) AS puntos " +
        "									FROM puntos_por_usuario a" +
        "                                   WHERE a.nombre is not null " +
        "									GROUP BY a.nombre " +
        "									ORDER BY puntos desc ";

		dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;   
        reader = dbcmd.ExecuteReader();
        

        while (reader.Read()){
           print("registra");
           print(reader.GetString(0)+""+ reader.GetInt32(1));
           PuntosUsuario usuario = new PuntosUsuario(reader.GetString(0), reader.GetInt32(1)); 
           ranking.Add(usuario);
           
			}
        print(ranking.Count);

        reader.Close();
        reader = null;

			return ranking;
						
		}
    public Boolean validarPass(string usuario ,string pass) {
        usuario = "\"" + usuario + "\"";
        pass = "\"" + pass + "\"";
        Boolean exito = false;
        consulta = "SELECT 1 " +
         "          FROM " + Tabla.USUARIO +
         "          WHERE " + Usuario.NOMBRE + " = " + usuario+
                    " AND "+ Usuario.PASS +" = " + pass;
        

        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;
        reader = dbcmd.ExecuteReader();

        exito = reader.Read();

        reader.Close();
        reader = null;
        return exito;
       
    }

		public void superarNivel(string usuario, int nivel){
			   usuario = "\"" + usuario + "\"";
       	 consulta = "UPDATE puntos_por_usuario " +
            		" SET superado = " + 1 +
            		" WHERE nivel = " + nivel +"" +
            		" AND nombre = "+ usuario ;
        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;
        dbcmd.ExecuteNonQuery();


		}

    public void insertarUsuario(string nombre,string pass) {

        nombre = "\"" + nombre + "\"";
        pass = "\"" + pass + "\"";
        consulta = "INSERT INTO " + Tabla.USUARIO + "(" + Usuario.NOMBRE + ","+ Usuario.PASS+ ") " +
                	" VALUES(" + nombre + "," + pass + ")";
        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;
        dbcmd.ExecuteNonQuery();
    }
    public int obtenerUltimoNivel(String usuario) {
        usuario = "\"" + usuario + "\"";
        int res = 0;
        string consulta = "SELECT MAX(a.nivel) " +
            "               FROM puntos_por_usuario a" +
            "               WHERE superado = 1 and a.nombre = " + usuario;

        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;
        reader = dbcmd.ExecuteReader();

        if (reader.Read())
        {
            res = reader.GetInt32(0);
        }
        reader.Close();
        reader = null;
        return res;

    }
    public string obtenerUsuarioActivo() {
        consulta = "SELECT "+ Usuario.NOMBRE+
					" FROM "+Tabla.USUARIO +
    				" WHERE "+ Usuario.ACTIVO+" = 1";  
        string usuario = "";

        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;
        reader = dbcmd.ExecuteReader();

        while (reader.Read()) {
            usuario = reader.GetString(0);
        }
				reader.Close();
        reader = null;

        return usuario;
    }

    public void hacerActivoUsuario(string usuario) {
        usuario = "\"" + usuario + "\"";
		sacarUsuarioActivo ();
		//se asigna activo al usuario
        consulta = "UPDATE " + Tabla.USUARIO +
        			" SET "+ Usuario.ACTIVO +" = 1"+
            		" WHERE "+ Usuario.NOMBRE+" = " + usuario;

        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;
        dbcmd.ExecuteNonQuery();
    }
	

	public void sacarUsuarioActivo() {
		//Se setea todos en 0
		consulta = "UPDATE " + Tabla.USUARIO +
					" SET " + Usuario.ACTIVO + " = 0";

		dbcmd = dbcon.CreateCommand();
		dbcmd.CommandText = consulta;
		dbcmd.ExecuteNonQuery();
	}

    public void agregarPuntaje(string usuario,int nivel,int puntaje) {
        usuario = "\"" + usuario + "\"";
        consulta = "INSERT INTO puntos_por_usuario " +
            		" VALUES("+ usuario +","+ nivel+"," + puntaje + ", 0)";
        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;
        dbcmd.ExecuteNonQuery();

    }

    public void modificarPuntaje(string usuario, int nivel, int puntaje) {
        usuario = "\"" + usuario + "\"";
        consulta = "UPDATE puntos_por_usuario " +
            		" SET puntaje = " + puntaje +
            		" WHERE nivel = " + nivel +"" +
            		" AND nombre = "+ usuario ;
        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;
        dbcmd.ExecuteNonQuery();

    }

    public int obtenerPuntaje(string usuario, int nivel) {
        //retorna -1 si no tiene puntaje
        usuario = "\"" + usuario + "\"";
        int puntaje = -1;
        consulta = "SELECT puntaje FROM puntos_por_usuario " +
            		" WHERE nombre = "+ usuario +
            		" AND nivel = "+ nivel;

        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;
        reader = dbcmd.ExecuteReader();


        while (reader.Read()) {

            puntaje = reader.GetInt32(0);
        }

        reader.Close();
        reader = null;

        return puntaje;

    }

    public string retornarUsuario() {
        string usuario = "";
				consulta = "";
        consulta = "SELECT nombre FROM usuario";
        dbcmd = dbcon.CreateCommand();
        dbcmd.CommandText = consulta;
        reader = dbcmd.ExecuteReader();


        while (reader.Read()) {

            usuario = reader.GetString(0);
        }

        reader.Close();
        reader = null;

        return usuario;
    }

}
