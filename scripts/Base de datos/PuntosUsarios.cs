﻿using System;

public class PuntosUsuario
{
		private string nombre;
    private int puntos;

		public PuntosUsuario(string nom,int pts){
			nombre = nom;
			puntos = pts;
		}
		
		public string getNombre(){
			return nombre;
		}
		
		public int getPuntos(){
			return puntos;
		}
		public void setPuntos(int pts){
			puntos = pts;
		}
		public void setNombre(string nom){
			nombre = nom;
		}
}
