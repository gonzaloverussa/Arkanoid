﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class Main : MonoBehaviour
{
    private string usuario;
    AccesoDB db = null;
    
    void Awake(){
//		string c1, c2;
//		c1 = "\"pepe333rina\"";
//		c2 = "\"pepa\"";
//		print ("algo");

		db = GetComponent<AccesoDB>();
	}

    public List<PuntosUsuario> obtenerRanking()
    {
        db.OpenDB();
        List<PuntosUsuario> ranking = db.obtenerPuntajes();
        db.CloseDB();
        return ranking;
    }
    public int obtenerUltimo () {
        int nivel;
        db.OpenDB();
        nivel=db.obtenerUltimoNivel(db.obtenerUsuarioActivo());
        db.CloseDB();
        return nivel;
    }
	public void crearUsuario(string usuario, string contrasenia){
		db.OpenDB ();
		db.insertarUsuario (usuario, contrasenia);
		db.CloseDB ();
	}

	public bool existeUsuario(string usuario){
		bool respuesta;
		db.OpenDB ();
		respuesta=	db.existeUsuario(usuario);
		db.CloseDB();
		return respuesta;
	}

	public bool validarUsuario(string usuario,string pass){
		bool respuesta;
		db.OpenDB ();
		respuesta=	db.validarPass(usuario ,pass);
		db.CloseDB();
		return respuesta;
	}

    public bool actualizarPuntos(int puntosNuevos)

    {
        bool nuevoRecord = false;
        int pts;
        db.OpenDB();
        string usuario = db.obtenerUsuarioActivo();
        pts = db.obtenerPuntaje(usuario, CrearBloques.nivel);
        if(pts == -1)
        {
            db.agregarPuntaje(usuario, CrearBloques.nivel, puntosNuevos);
            nuevoRecord = true;
        }
        else
        {
            if (pts < puntosNuevos){
                db.modificarPuntaje(usuario, CrearBloques.nivel, puntosNuevos);
                nuevoRecord = true;
            }
        }
        db.CloseDB();
        return nuevoRecord;



    }

    public void ganarNivel()
    {
        db.OpenDB();
        string usuario = db.obtenerUsuarioActivo();
        db.superarNivel(usuario, CrearBloques.nivel);
        db.CloseDB();
    }

    public void desactivarUsuarios()
    {
        db.OpenDB();
        db.sacarUsuarioActivo();
        db.CloseDB();
    }

	public void hacerActivoUsuario(string usuario) {
		db.OpenDB ();
		db.hacerActivoUsuario (usuario);
		db.CloseDB();
	}

	public void salirUsuario() {
		db.OpenDB ();
		db.sacarUsuarioActivo ();
		db.CloseDB();
	}

	public bool existeUsuarioActivo() {
		bool respuesta = false;
		inicializar ();
		db.OpenDB ();
//		print ("existeUsuarioActivo: " + db.obtenerUsuarioActivo());
		if (db.obtenerUsuarioActivo () != "") {
			respuesta = true;
		}
		db.CloseDB();
		return respuesta;
	}

	private void inicializar(){
//		print ("db = " + db.ToString ());
		if (db == null) {
			db = GetComponent<AccesoDB> ();
		}
	}
}
