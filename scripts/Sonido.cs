﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Sonido : MonoBehaviour {
    public Button myButton;
    public Sprite soundOn;
    public Sprite soundOff;
    public AudioSource myAudioSource;
    bool music_on = true;

    void Start() {
        myAudioSource = GetComponent<AudioSource>();
      myButton= GetComponent<Button>();
    }
    public void Pause() {
        if (music_on)
        {//si esta reproduciendose el sonido
            myAudioSource.Pause();
            myButton.image.overrideSprite = soundOff;
        }
        else
        {
            myAudioSource.UnPause();
            myButton.image.overrideSprite = soundOn;
        }
        music_on = !music_on;
    }
    void Update(){

       }

}

