﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour
{
    public InputField usuario;
    public InputField contrasenia;
    private string tUsuario;
    private string tConstraseña;
    public Text errorUsuario;
    public Text errorContrasenia;
    public CambioPanel CambioPanel;
    public Main funciones;
    private InputField ifusuario, ifpass;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (usuario.isFocused)
            {
                contrasenia.Select();
            }
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            LoginButton();
        }

    }
    void Start()
    {
        if (funciones.existeUsuarioActivo())
        {
            //ya se encuentra un usuario activo
            CambioPanel.Menu();
        }
        else
        {
            CambioPanel.Login();
        }

    }
    public void LoginButton()
    {
        errorUsuario.text = "";
        errorContrasenia.text = "";
        tUsuario = usuario.text;
        tConstraseña = contrasenia.text;
        if (ValidarUsuario() && ValidarContrasenia())
        {//Login correcto, sigo con el siguiente menu
            funciones.desactivarUsuarios();
            funciones.hacerActivoUsuario(tUsuario);
            CambioPanel.Menu();
            usuario.text = "";
            contrasenia.text = "";
            errorUsuario.text = "";
            errorContrasenia.text = "";
        }
        contrasenia.text = "";
    }




    private bool ValidarUsuario()
    {
        bool un = false;
        if (tUsuario != "")
        {
            //si el usuario no esta incompleto.
            if (funciones.existeUsuario(tUsuario))
            {
                un = true;
                Debug.LogWarning("Usuario Correcto"); ;
                errorUsuario.text = "";
            }
            else
            {
                //si no existe el usuario en la base de datos.
                errorUsuario.text = "Usuario incorrecto";
                Debug.LogWarning("No existe el usuario");
            }
        }
        else
        {//si el campo usuario esta incompleto.
            errorUsuario.text = "Usuario incompleto";
            Debug.LogWarning("Usuario incompleto");
        }
        return un;
    }

    private bool ValidarContrasenia()
    {
        bool pw = false;
        if (tConstraseña != "")
        {
            //si la Contrasenia no esta incompleta.
            if (funciones.validarUsuario(tUsuario, tConstraseña))
            {
                pw = true;
            }
            else
            {
                Debug.LogWarning("Contrasenia Incorrecta.");
                errorContrasenia.text = "Contraseña Incorrecta";
            }
        }
        else
        {
            errorContrasenia.text = "Contraseña Incompleta";
            Debug.LogWarning("Contrasenia incompleta");
        }
        return pw;
    }
}
