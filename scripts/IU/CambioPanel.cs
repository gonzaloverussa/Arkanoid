﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CambioPanel : MonoBehaviour {
    public GameObject panelLogin;
    public GameObject panelRegister;
    public GameObject panelMenu;
    public GameObject panelLevel;
    public GameObject panelRanking;
	public GameObject loader;
	public Slider barraDeCarga;
	public Text porcentaje;
    private Main funciones;

    private void Start() {
		funciones = GetComponent<Main>();
		Login ();
    }

    public void Login() {
        panelRanking.SetActive(false);
        panelMenu.SetActive(false);
        panelLevel.SetActive(false);
        panelRegister.SetActive(false);
		panelLogin.SetActive(true);  
		loader.SetActive (false);    
    }

    public void Register() {
        panelRanking.SetActive(false);
        panelMenu.SetActive(false);
        panelLevel.SetActive(false);
        panelLogin.SetActive(false);
		panelRegister.SetActive(true);
		loader.SetActive (false);
    }

    public void Niveles() {
        panelRanking.SetActive(false);
        panelMenu.SetActive(false);
        panelLogin.SetActive(false);
        panelRegister.SetActive(false);
		panelLevel.SetActive(true);
		loader.SetActive (false);
    }

    public void Menu() {
        panelRanking.SetActive(false);
        panelLogin.SetActive(false);
        panelLevel.SetActive(false);
        panelRegister.SetActive(false);
		panelMenu.SetActive(true);
		loader.SetActive (false);
    }

    public void Ranking() {
        panelLogin.SetActive(false);
        panelLevel.SetActive(false);
        panelRegister.SetActive(false);
        panelMenu.SetActive(false);
		panelRanking.SetActive(true);
		loader.SetActive (false);
    }

	public void Carga() {
		StartCoroutine(loading("escenas/Juego"));
		panelLogin.SetActive(false);
		panelLevel.SetActive(false);
		panelRegister.SetActive(false);
		panelMenu.SetActive(false);
		panelRanking.SetActive(false);
		loader.SetActive (true);
	}

	public void Salir() {
		funciones.salirUsuario();
		Login ();
	}

    public void salirJuego() {
        Application.Quit();
    }

	IEnumerator loading(string scene) {
		AsyncOperation operacion = SceneManager.LoadSceneAsync (scene);
		while (!operacion.isDone) {
			float progreso = Mathf.Clamp01 (operacion.progress / 0.9f);
			barraDeCarga.value = progreso;
			porcentaje.text = progreso * 100 + "%";
			yield return null;
		}
	}
}
