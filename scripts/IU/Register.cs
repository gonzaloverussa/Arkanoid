﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text.RegularExpressions;

public class Register : MonoBehaviour
{	public InputField usuario;
	public InputField contrasenia;
	public InputField confContrasenia;
	public Text errorUsuario;
	public Text errorContrasenia;
	public Text errorConfContrasenia;
	private string tUsuario;
	private string tContrasenia;
	private string tConfContrasenia;
    public CambioPanel CambioPanel;
	public Main funciones;
    // Use this for initialization
    void Start()
    {

    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (usuario.isFocused)
            {
                contrasenia.Select();
            }
            if (contrasenia.isFocused)
            {
                confContrasenia.Select();
            }
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            RegisterButton();
        }

    }
    public void RegisterButton()
	{
		errorUsuario.text = "";
		errorContrasenia.text="";
		errorConfContrasenia.text="";	
		tUsuario = usuario.text;
		tContrasenia= contrasenia.text;
		tConfContrasenia = confContrasenia.text;
		if(ValidarUsuario() && ValidarContrasenia() && ValidarConfirmacion()) 
		{//Registro correcto correcto, sigo con el siguiente menu.
			usuario.text = "";
			contrasenia.text= "";
			confContrasenia.text = "";
			errorUsuario.text = "";
			errorContrasenia.text="";
			errorConfContrasenia.text="";
			funciones.crearUsuario(tUsuario,tContrasenia);
            funciones.desactivarUsuarios();
            funciones.hacerActivoUsuario (tUsuario);
			CambioPanel.Menu();
}

    }



    private bool ValidarUsuario()
    {
        bool un = false;
        if (tUsuario != "")
        {//si el usuario no esta incompleto.

			if (funciones.existeUsuario(tUsuario))
            { //el usuario esta en uso
				usuario.text = "";
				errorUsuario.text="Usuario no disponible";
                Debug.LogWarning("Usuario no disponible");
            }
            else
            {
                //si no existe el usuario en la base de datos.
                Debug.LogWarning("Usuario Correcto");
				errorUsuario.text = "";
                un = true;
            }
        }
        else
        {//si el campo usuario esta incompleto.
            Debug.LogWarning("Usuario incompleto");
			errorUsuario.text="Usuario incompleto";

        }
        return un;
    

    }

    private bool ValidarContrasenia()
    {
        bool pw = false;
        if (tContrasenia != "")
        {//si la contraseña no esta incompleta.
			if (tContrasenia.Length >= 4 && tContrasenia.Length <= 20 ) { //si la contraseña tiene entre 4 y 20 caracteres
				Debug.LogWarning("Contraseña correcta");
				errorContrasenia.text = "";
                pw = true;
            }
            else
			{	errorContrasenia.fontSize = 25;
				contrasenia.text = "";
				errorContrasenia.text="Tiene que tener entre 4 y 20 caracteres";
                Debug.LogWarning("La Contraseña Tiene que tener entre 4 y 20 caracteres.");
            }
        }
        else
		{	contrasenia.text = "";
			errorContrasenia.fontSize = 44;
			errorContrasenia.text="Contraseña incompleta";
            Debug.LogWarning("Contraseña incompleta");

        }
        return pw;
    }



 	  private bool ValidarConfirmacion() {
        bool cpw = false;
		if (tConfContrasenia != "") 
		{//si la confirmacion de la contraseña no esta incompleta.
			if (tConfContrasenia.Length >= 4 && tConfContrasenia.Length <= 20) 
			{ //si la confirmmacion de contraseña tiene entre 4 y 20 caracteres
				if (tConfContrasenia == tContrasenia) 
				{//si la contraseña es igual a la confirmacion.
					Debug.LogWarning ("Contraseña verificada.");
					errorConfContrasenia.text = "";
					cpw = true;
				}
				else {//Las contraseñas ingresadas son distintas.
					confContrasenia.text = "";
					errorContrasenia.fontSize = 44;
					errorConfContrasenia.text="Contraseñas distintas";
					Debug.LogWarning ("Contraseñas distintas");
				}
			} 
			else 
			{
				confContrasenia.text = "";
				errorConfContrasenia.text="Contraseñas distintas";
				Debug.LogWarning("La  Confirmacion de Contraseña Tiene que tener entre 4 y 20 caracteres.");
			}
		}
        else
		{	confContrasenia.text = "";
			errorContrasenia.fontSize = 44;
			errorConfContrasenia.text="Contraseñas distintas";
            Debug.LogWarning("Verificacion de Contraseña incompleta");
        }
        return cpw;
    }
}
