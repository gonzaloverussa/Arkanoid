﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuJuego : MonoBehaviour {
    public GameObject panelMenu;
    public GameObject panelGanar;
    public GameObject panelPerder;
    private bool estadoMenu=false;
    public Text nuevoRecordGanar;
    public Text nuevoRecordPerder;

    // Use this for initialization
    void Start () {
        panelPerder.SetActive(false);
        panelGanar.SetActive(false);
        panelMenu.SetActive(false);
   
	}

    private void Awake()
    {
        panelPerder.SetActive(false);
        panelGanar.SetActive(false);
        panelMenu.SetActive(false);
    }
    public void menuDesplegable() {
        panelMenu.SetActive(!estadoMenu);
        estadoMenu = !estadoMenu;
        if (estadoMenu) { Time.timeScale = 0; }
        else { Time.timeScale = 1; }
    }

    public void reiniciar() {
		CrearBloques.nivel = CrearBloques.nivel;
        SceneManager.LoadScene("escenas/Juego");
        Time.timeScale = 1;
    }
    public void salir() {
        SceneManager.UnloadSceneAsync("escenas/Juego");
        SceneManager.LoadScene("escenas/Menu");
        Time.timeScale = 1;
    }

    public void siguienteNivel()
    {   if (CrearBloques.nivel<10)
        {
            CrearBloques.nivel = CrearBloques.nivel + 1;
            SceneManager.UnloadSceneAsync("escenas/Juego");
            SceneManager.LoadScene("escenas/Juego");
        }
        Time.timeScale = 1;
    }

    public void perderPartida(bool record) {
        print(record);
        nuevoRecordPerder.gameObject.SetActive(record);
        panelPerder.SetActive(true);
        panelMenu.SetActive(false);
        
        Time.timeScale = 0;
    }

    public void ganarPartida(bool record) {
        print(record);
        nuevoRecordGanar.gameObject.SetActive(record);
        panelGanar.SetActive(true);
        panelMenu.SetActive(false);
        Time.timeScale = 0;
    }

    
}
