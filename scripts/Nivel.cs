﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Nivel : MonoBehaviour {
    public Button[] botones;
    public Sprite levelBloqueado;
    public Sprite levelDesbloqueado;
    public Main funciones;
	public CambioPanel cambioPanel;

	void Start() {
		activarBotones();
	}

	public void doNivel(int nivel) {
		CrearBloques.nivel = nivel;
		cambioPanel.Carga ();
	}

    public void activarBotones() {
        foreach (Button i in botones) {
            i.interactable = false;
        }
        int ultimoNivel = funciones.obtenerUltimo();
        if (ultimoNivel == 12) {
            ultimoNivel = 11;
        }
        for (int i=0; ultimoNivel >= i; i++) {
            botones[i].image.overrideSprite = levelDesbloqueado;
            botones[i].interactable = true;
        }
    }
}