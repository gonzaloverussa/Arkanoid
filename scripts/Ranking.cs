﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ranking : MonoBehaviour {
    public GameObject jugador;
    public Main funciones;

    // Use this for initialization
    void Start () {
        List<PuntosUsuario> ranking;
        Text usuario;
        Text puntos;
        ranking = funciones.obtenerRanking();
        GameObject puntaje;
        float basex,basey;
        basex = 0;
        basey = 320;
        print(ranking.Count);
        foreach (PuntosUsuario i in ranking)
        {
            RectTransform rect;
            print(basey);
            puntaje = Instantiate(jugador, new Vector2(basex, basey), Quaternion.identity, gameObject.transform);
            //puntaje.transform.parent = gameObject.transform;
            // puntaje.transform.position.x = 

            puntaje.GetComponent<RectTransform>().localPosition = new Vector3(basex, basey,0f);
            basey -= 30;

            usuario = puntaje.transform.GetChild(0).GetComponent<Text>();
            puntos = puntaje.transform.GetChild(1).GetComponent<Text>();
            usuario.text = i.getNombre();
            puntos.text = i.getPuntos().ToString();
        }
            }
	
	// Update is called once per frame
	void Update () {
		
	}
}
